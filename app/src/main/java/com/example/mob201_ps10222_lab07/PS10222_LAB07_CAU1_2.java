package com.example.mob201_ps10222_lab07;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class PS10222_LAB07_CAU1_2 extends AppCompatActivity {

    Button btn_all, btn_doraemon, btn_nobita, btn_rotation, btn_moving, btn_zoom;
    ImageView iv_btnNext, iv_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab07__cau1_2);
        AnhXa();

        btn_all.setOnClickListener(new setOnClickListener());
        btn_doraemon.setOnClickListener(new setOnClickListener());
        btn_nobita.setOnClickListener(new setOnClickListener());
        btn_rotation.setOnClickListener(new setOnClickListener());
        btn_moving.setOnClickListener(new setOnClickListener());
        btn_zoom.setOnClickListener(new setOnClickListener());
        iv_btnNext.setOnClickListener(new setOnClickListener());
    }

    private class setOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                // Câu 1:
                case R.id.btn_rotation:
                    int dest = 360;
                    if (iv_all.getRotation() == 360){
                        System.out.println(iv_all.getAlpha());
                        dest = 0;
                    }
                    ObjectAnimator animator = ObjectAnimator.ofFloat(iv_all,"rotation", dest);
                    animator.setDuration(2000);
                    animator.start();
                    return;
                    
                case R.id.btn_moving:
                    ObjectAnimator anim_move = ObjectAnimator.ofFloat(iv_all, "translationX", 200f);
                    anim_move.setDuration(2000);
                    anim_move.start();
                    return;
                    
                case R.id.btn_zoom:
                    Animation anim_zoom = AnimationUtils.loadAnimation(PS10222_LAB07_CAU1_2.this,R.anim.zoom);
                    iv_all.startAnimation(anim_zoom);
                    return;

                // Câu 2:
                case R.id.btn_choose_all:
                    showImage("All");
                    return;

                case R.id.btn_choose_doraemon:
                    showImage("Doraemon");
                    return;

                case R.id.btn_choose_nobita:
                    showImage("Nobita");
                    return;

                case R.id.btn_next:
                    Intent intent = new Intent(PS10222_LAB07_CAU1_2.this, PS10222_LAB07_CAU3.class);
                    startActivity(intent);
                    return;
            }
        }
    }

    private void showImage(String img) {
        // Hide image
        ObjectAnimator anim = ObjectAnimator.ofFloat(iv_all, "translationX", 0f, 500f);
        anim.setDuration(2000);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(iv_all, "alpha", 1f, 0f);
        anim1.setDuration(2000);

        //Show image
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(iv_all, "translationX", -500f, 0f);
        anim2.setDuration(2000);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(iv_all, "alpha", 0f, 1f);
        anim3.setDuration(2000);

        // Config slideshow process to show next image
        AnimatorSet ans = new AnimatorSet();
        ans.play(anim2).with(anim3).after(anim).after(anim1);
        ans.start();
        final String imgName = img;

        // Set listener for determine when animation finished
        anim1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                // Chang source of ImageView
                if (imgName == "Nobita"){
                    iv_all.setImageResource(R.drawable.nobita);
                }
                if (imgName == "Doraemon"){
                    iv_all.setImageResource(R.drawable.doraemon);
                }
                if (imgName == "All"){
                    iv_all.setImageResource(R.drawable.all);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void AnhXa() {
        btn_all = (Button)findViewById(R.id.btn_choose_all);
        btn_doraemon = (Button)findViewById(R.id.btn_choose_doraemon);
        btn_nobita = (Button)findViewById(R.id.btn_choose_nobita);
        btn_rotation = (Button)findViewById(R.id.btn_rotation);
        btn_moving = (Button)findViewById(R.id.btn_moving);
        btn_zoom = (Button)findViewById(R.id.btn_zoom);

        iv_btnNext = (ImageView) findViewById(R.id.btn_next);
        iv_all = (ImageView) findViewById(R.id.imv_all);
    }
}
